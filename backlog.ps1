
<# 
    Copyright 2015 Tyler Manifold

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#>

<#
 This is a simple script to create 7-Zip Archives to function
  as backups for files.
   
 It is recommended to use Windows Task Scheduler to automatically run this script 
  at regular intervals.
#>


param(

	# Add a directory to backlog.dat
	[string]$add,
	
	# Begin backup sequence
	[switch]$backup,

    # create an incremental backup
    [switch]$incremental,

    # set a name for the archive
    [string]$name,
	
	# List the contents of backlog.dat
	[switch]$list,
	
	# Remove an entry from backlog.dat
	[int]$remove,
	 
	# Displays the most recent logfile
	[switch]$log,
	
	# Specify target location instead of using default
	[string]$target,
	
	# Display help dialog
	[switch]$help
)

$VERSION = 2

# Gets a variable from the ini file at line_num

function GetINIValue {

    param ([int] $line_num)

    $equ_index = $ini_file[$line_num].IndexOf("=")
   
    $val = $ini_file[$line_num].Substring($equ_index + 1, $ini_file[$line_num].Length - $equ_index - 1)

    return $val
}

# Initialize backlog.ini

if (-NOT (Test-Path $PSScriptRoot\backlog.ini)) {

    echo "7zip=C:\Program Files\7-zip\7z.exe" > $PSScriptRoot\backlog.ini
    echo "Destination=C:$env:HomePath\backlog\" >> $PSScriptRoot\backlog.ini
    echo "DeleteOldFiles=False" >> $PSScriptRoot\backlog.ini
    echo "MaxFileAge=30" >> $PSScriptRoot\backlog.ini

    $ini_file = Get-Content $PSScriptRoot\backlog.ini

} else {

    $ini_file = Get-Content $PSScriptRoot\backlog.ini
}


$szip = GetINIValue 0
$destination = GetINIValue 1
$delete_old  = GetINIValue 2
$delete_age  = GetINIValue 3

Set-Alias SZip $szip

$date 		 = Get-Date -Format yyyy.MM.dd
$destination = $destination
$secondary   = "C:$env:HomePath\backlog\$date"

#>
if (-NOT (Test-Path $destination)) {

    $destination = $secondary
}

# Create the directory list if it doesn't exist

if (Test-Path $PSScriptRoot\backlog.dat){

     $source = Get-Content $PSScriptRoot\backlog.dat

} else {
		 
	echo $null > backlog.dat
}

if ($incremental) {

    # assign a name to the archive if one is provided

    if ($name) {

        $archive_name = $name
    
    } else {

        $archive_name = "incremental"
    }

    # verify target directory exists

    if (! (Test-Path $destination\$archive_name)) { mkdir $destination\$archive_name }

    # create the log file

    $logfile = "$destination\$archive_name\$date.back.log"
    $null > $logfile

    # collect source items into an ArrayList for backup

    $items = New-Object System.Collections.ArrayList

    foreach ($line in $source) {

        $items.Add($line)
    }

    # Begin Incremental backup.
    #  Updates only files that:
    #    - Exist on disk but not in the archive
    #    - Are newer on the disk than in the archive

    SZip a -mx9 -spf -up0q0r2x0y2z0w2 "$destination\$archive_name\$archive_name.7z" $items -bb3 | tee -file $logfile

    if ($delete_old -and $delete_age -gt 0) {

        # Delete logfiles older than $delete_age

        $file_age = (Get-Date).AddDays(-$delete_age)
    
        if (Test-Path $destination\$archive_name) {

            Write-Output "Deleting log files older than $delete_age days....." >> $logfile
     
            $files = Get-ChildItem $destination\$archive_name | Where {$_.LastWriteTime -le "$file_age"}

            foreach ($file in $files) {

                if ($file.Exists) {

                     if ($file -ne $null) {

                        Write-Output "Deleting $file"
                        Remove-Item -Recurse $file.FullName | Out-Null
                    }
                }
                
            }

            Write-Output "Done." >> $logfile
        }
    }
}

if ($backup) {

    # assign a name to the archive if one is provided

    if ($name) {

        $archive_name = $name
    
    } else {

        $archive_name = $date
    }

	# Make sure the target directory exists
	
	if (!(Test-Path $destination\$archive_name)) { mkdir $destination\$archive_name }
	
	# Create the log file
	
	$logfile = "$destination\$archive_name\$archive_name.back.log"
    $null > $logfile
	
    # Collect items into an ArrayList for backup

    $items = New-Object System.Collections.ArrayList

    foreach ($line in $source) {

        $items.Add($line)
    }

	# Begin backup process with 7z

    SZip a -mx9 -spf "$destination\$archive_name\$archive_name.7z" $items -bb3 | tee -file $logfile
	
	#SZip l $destination\$date\$date.7z >> $logfile

    if ($delete_old -and $delete_age -gt 0) {

        # Delete backups older than $delete_age

        $file_age = (Get-Date).AddDays(-$delete_age)
    
        if (Test-Path $destination) {

            Write-Output "Checking '$destination' for files older than $delete_age days....." >> $logfile
     
            $files = Get-ChildItem $destination | Where {$_.LastWriteTime -le "$file_age"}

            foreach ($file in $files) {

                if ($file.Exists) {

                     if ($file -ne $null) {

                        Write-Output "Deleting $file"
                        Remove-Item -Recurse $file.FullName | Out-Null
                    }
                }
                
            }

            Write-Output "Done."
        }
    }
}

if ($target) {

	$destination = $target
}

if ($add) {

	Write-Output $add >> backlog.dat
}

if ($remove) {

	# Still not sure why the hell this works, but it does the trick. Thanks, StackOverflow.

	$source | foreach {$n=1}{if ($n++ -ne $remove) {$_}} > $PSScriptRoot\backlog.dat
}

if ($list) {

	# Make sure backlog.dat exists. This may or may not be redundant *shrug*

	if (Test-Path $PSScriptRoot\backlog.dat) {

		# Step through entries in backlog.dat and display them in a numbered list.

		$t = 1
		
		foreach ($i in $source) {

			Write-Output "$t. $i"
			$t++
		}

	} else {

		Write-Output "WARNING: backlog.dat not found in $PSScriptRoot\" >> $logfile 
	}
}

if ($log) {

	# Get the log file from the most recent backup directory

	$folder = Get-ChildItem "$destination" | select -last 1
	cat $destination\$folder\*.log
}

if ($help) {

	Write-Output ""
	Write-Output "Name:  Backlog"
    Write-Output "Version: $version"
	Write-Output "Usage: backlog [OPTION]... [PATH]"
	Write-Output "Use 7-zip to create an archive of directories and files specified"
	Write-Output " in backlog.dat at maximum compression ratio."
	Write-Output ""
	Write-Output "  -backup         Begin the backup sequence"
    Write-Output "  -incremental    Begin an incremental backup."
    Write-output "                   Mutually exclusive from -backup" 
	Write-Output "  -target         Specify a target directory for backup"
	Write-Output "                   instead of using default."
	Write-Output "                   Use in conjunction with -backup"
	Write-Output "  -name           Specify a name for the archive. If no"
    Write-Output "                   name is specified, the default will be"
    Write-Output "                   used."
    Write-Output ""
	Write-Output "  -add            Write new entry to backup list"
	Write-Output "  -remove         Delete an entry from the backup list"
	Write-Output ""
	Write-Output "  -list           Display the backup list"
	Write-Output "  -log            Display the most recent log file"
	Write-Output "  -help           Display this help dialog"
	Write-Output ""
}


# SIG # Begin signature block
# MIIEOAYJKoZIhvcNAQcCoIIEKTCCBCUCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUewP5VfLpUjl0297gVU6AgjpT
# lmCgggJCMIICPjCCAaugAwIBAgIQTBY+WmzsHaJFNaZXg+taVzAJBgUrDgMCHQUA
# MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
# Fw0xNzA1MDEwMTAwMDlaFw0zOTEyMzEyMzU5NTlaMB8xHTAbBgNVBAMTFFR5bGVy
# IFBvd2VyU2hlbGwgQ1NDMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDsCHD/
# Q6apEudoQaFcBCasTWagOMMh0ctC0XjlnQ/kp3m2i7UFsiSAHymERPKSmw3hCttE
# pGgKSV6prHEyWkYiZ7qzErVfuSX467udc+MDoB2TkI/fjLjgp4ObIJB4l6UkRfem
# V50Qhrj6MbLcEILhlKBVOGtAf6pHglN/z2MKcQIDAQABo3YwdDATBgNVHSUEDDAK
# BggrBgEFBQcDAzBdBgNVHQEEVjBUgBDT0hF1Rt6xAoZ6rqjioC1poS4wLDEqMCgG
# A1UEAxMhUG93ZXJTaGVsbCBMb2NhbCBDZXJ0aWZpY2F0ZSBSb290ghAJdVNImwyA
# v0DHQgK8NQvWMAkGBSsOAwIdBQADgYEAow2rFuL9jSGr5PhnacFyFg12d2m/I2mZ
# cUIMRdH90KBxwy6b4dWk8PWe5wtQ136dh0EpoIbMcfV0woP2GBnSr6VUigxpidq7
# r7R/uawLmkf3PqqjWdcPTgQ+3bBjlky7S2sXNbxi+Ou6SsBDqCEM4lR16SwpxK9K
# ut5tlWWkLEYxggFgMIIBXAIBATBAMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9j
# YWwgQ2VydGlmaWNhdGUgUm9vdAIQTBY+WmzsHaJFNaZXg+taVzAJBgUrDgMCGgUA
# oHgwGAYKKwYBBAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYB
# BAGCNwIBBDAcBgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0B
# CQQxFgQUhEUlAdAdaA0otT3F1tXcjI8hv7cwDQYJKoZIhvcNAQEBBQAEgYA5WysC
# SQQJ/+HyeS4K/Wa488NabdiZm5rTidcRU0tTr9hewWPcn4nrgvezktDFnAXqm6wi
# q/0G1v+HFnelnZItMl+Fl0Qi6ag5A7dZDlbkyD93iJrofc7uxPFh8roYwHgh9f81
# soZuT4A5thlJajaHOAevjwDSCsJQuwHRi+TghQ==
# SIG # End signature block
